import { Component } from "react";
import TitleText from "./text/TitleText";

class TitleComponent extends Component {
    render() {
        return (
            <TitleText />
        )
    }
}

export default TitleComponent;