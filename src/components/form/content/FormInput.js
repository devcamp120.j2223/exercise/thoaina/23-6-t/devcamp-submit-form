import { Component } from "react";


class FormInput extends Component {
    formChangeHandle(event) {
        console.log(event.target.value);
    }

    btnClickHandle() {
        console.log("Form đã được submit");
    }

    render() {
        return (
            <div className="main-form">
                <div className="row m-3 form-group">
                    <div className="col-4">
                        <label>First Name</label>
                    </div>
                    <div className="col-8">
                        <input className="form-control" placeholder="Your name..." onChange={this.formChangeHandle} />
                    </div>
                </div>
                <div className="row m-3 form-group">
                    <div className="col-4">
                        <label>Last Name</label>
                    </div>
                    <div className="col-8">
                        <input className="form-control" placeholder="Your last name..." onChange={this.formChangeHandle} />
                    </div>
                </div>
                <div className="row m-3 form-group">
                    <div className="col-4">
                        <label>Country</label>
                    </div>
                    <div className="col-8">
                        <select className="form-control" onChange={this.formChangeHandle}></select>
                    </div>
                </div>
                <div className="row m-3 form-group">
                    <div className="col-4">
                        <label>Subject</label>
                    </div>
                    <div className="col-8">
                        <textarea className="form-control" placeholder="Write something..." rows={7} onChange={this.formChangeHandle} />
                    </div>
                </div>
                <div className="row m-3">
                    <div className="col-12">
                        <button className="btn btn-success" onClick={this.btnClickHandle}>Send Data</button>
                    </div>
                </div>
            </div>
        )
    }
}

export default FormInput;