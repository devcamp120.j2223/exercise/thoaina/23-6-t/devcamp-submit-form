import "bootstrap/dist/css/bootstrap.min.css";
import FormComponent from "./components/form/FormComponent";
import TitleComponent from "./components/title/TitleComponent";
import "./App.css";

function App() {
    return (
        <div className="App">
            <div className="container">
                <TitleComponent />
                <FormComponent />
            </div>
        </div>
    );
}

export default App;
